//import liraries
import React, { Component } from 'react';
import { View, Image, StyleSheet } from 'react-native';

// create a component
class CustomMapMarker extends Component {
    render() {
        return (
        <View style={{flex:1}}>
          <Image source={require('../assets/pin.png')} style={{width:35,height:35,borderRadius:100,
          borderColor:'green',borderWidth:2}}/>
        </View>
        );
    }
}



//make this component available to the app
export default CustomMapMarker;
