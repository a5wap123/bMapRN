/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Image,
  Text,
  View
} from 'react-native';
import MapView from 'react-native-maps';
import CustomMapMarker from './src/components/CustomMapMarker';
export default class bmap extends Component {

constructor(props){
  super(props);
  this.state = {
    loc:{
      latitude: 21.0183177,
      longitude: 105.8235946,
      latitudeDelta:0.5,
      longitudeDelta:0.5
    },
    
  }
}
componentWillMount() {
  
}
componentDidMount() {
}
__onMapReady = () => {
        this.aMapView.animateToRegion({
      latitude: 21.0183177,
      longitude: 105.8235946,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,},3000);
}
  render() {
    const { region } = this.props;
    console.log(region);

    return (
      <View style ={styles.container}>
        <MapView
          style={styles.map}
          showsUserLocation={true}
          userLocationAnnotationTitle='location'
          moveOnMarkerPress={false}
          onMarkerPress={(e)=>{console.log(e.nativeEvent)}}
          onCalloutPress={(e)=>{console.log(e.nativeEvent)}}
          animateToRegion={ this.state.loc }
          onMapReady={_onMapReady()}
          ref={(MapView) => { this.aMapView = MapView; }}
        >
          <MapView.Marker
          coordinate={this.state.loc}
          title='title'
          description='description'
        >
       <CustomMapMarker/>
        <MapView.Callout>
      <CustomMapMarker/>
    </MapView.Callout>
        </MapView.Marker>
        </MapView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    flex:1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});

AppRegistry.registerComponent('bmap', () => bmap);
